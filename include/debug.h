#ifdef OMPTRACING_DEBUG
#include <stdio.h>

#define UNINIT -128

static int DebugLevel = UNINIT;

#define DEBUGP(prefix, ...)                                                    \
  {                                                                            \
    fprintf(stderr, "%s --> ", prefix);                                        \
    fprintf(stderr, __VA_ARGS__);                                              \
  }

#define DP(...)                                                                \
  do {                                                                         \
    if (DebugLevel == UNINIT) {                                                \
      DebugLevel = 0;                                                          \
      char *envStr = getenv("OMPTRACING_DEBUG");                               \
      if (envStr != NULL) {                                                    \
        DebugLevel = atoi(envStr);                                             \
      }                                                                        \
    }                                                                          \
    if (DebugLevel > 0) {                                                      \
      DEBUGP("OmpTracing", __VA_ARGS__);                                       \
    }                                                                          \
  } while (0)
#else // OMPTRACING_DEBUG
#define DP(...)                                                                \
  {}
#endif // OMPTRACING_DEBUG
