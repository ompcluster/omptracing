#ifndef _OMPTRACING_H_
#define _OMPTRACING_H_

/*!
 * Tagging begin (indicates when an event begins). Similar to
 * omptracingTagEventBegin, but permits choose the tag_name too.
 * @param tag_name is the name of tagging.
 * @param event_name is the timeline event name associate with the tagging name.
 */
void omptracingTagBegin(char *tag_name, char *event_name);

/*!
 * Tagging end (indicates when an event ends). Similar to
 * omptracingTaggingEventEnd, but permits choose the tag_name too.
 * @param tag_name is the name of tagging.
 * @param event_name is the timeline event name associate with the tagging name.
 */
void omptracingTagEnd(char *tag_name, char *event_name);

/*!
 * Tagging event begin (indicates when an event begins).
 * @param event_name is the timeline event name associate with the tagging name.
 */
void omptracingTagEventBegin(char *event_name);

/*!
 * Tagging pop (indicates when an event ends).
 * @param event_name is the timeline event name associate with the tagging name.
 */
void omptracingTagEventEnd(char *event_name);

#endif // _OMPTRACING_H_
