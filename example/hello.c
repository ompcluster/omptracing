#include <omp.h>
#include <stdio.h>

void loop(int i) {
  int count, j;

/** Test depend callback. */
#pragma omp task depend(out : count)
  count = 0;
#pragma omp task depend(out : j)
  j = 0;

#pragma omp task depend(in : count, j)
  for (j = 0; j < 100000; j++)
    count = 10;
}

int main() {
/** Test master callback. */
#pragma omp parallel num_threads(2)
#pragma omp master
  loop(0);

/** Test task callback. */
#pragma omp parallel num_threads(4)
#pragma omp single
  {
    int i = 0;

    while (i < 5) {
#pragma omp task
      loop(i);
      i++;
    }
  }

  int a, b;

/** Test loop callback. */
#pragma omp parallel for num_threads(2)
  for (a = 0; a < 1000000; a++)
    b = 10;

  return 0;
}
