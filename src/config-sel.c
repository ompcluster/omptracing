#include "config-sel.h"

/*!
 * Set default callbacks selection.
 * @param config is the config selection data pointer.
 */
void set_default_callbacks(config_selection *config) {
  config->task = CONFIG_SELECTED;
  config->task_create = CONFIG_NOT_SELECTED;
  config->thread = CONFIG_SELECTED;
  config->parallel = CONFIG_SELECTED;
  config->work = CONFIG_NOT_SELECTED;
  config->implicit_task = CONFIG_NOT_SELECTED;
  config->master = CONFIG_NOT_SELECTED;
  config->sync_region = CONFIG_NOT_SELECTED;
  config->taskwait = CONFIG_NOT_SELECTED;
#ifdef OPENMP_TARGETS
  config->target = CONFIG_SELECTED;
  config->target_submit = CONFIG_SELECTED;
  config->target_data_op = CONFIG_SELECTED;
#endif
#ifndef OPENMP_TARGETS
  config->target = CONFIG_NOT_SELECTED;
  config->target_submit = CONFIG_NOT_SELECTED;
  config->target_data_op = CONFIG_NOT_SELECTED;
#endif
}

/*!
 * Get default config selection.
 * @param config is the config selection data pointer.
 */
void get_config_default(config_selection *config) {
  set_default_callbacks(config);
  config->max_tasks = MAX_TASKS_DEFAULT;
  config->graph_time = CONFIG_SELECTED;
  config->critical_path = CONFIG_NOT_SELECTED;
}

/*!
 * Get configuration selection from json.
 * @param buffer is the json config file in string format.
 * @param config is the pointer to config selection data.
 * @return CONFIG_ERROR if can't parse json file or CONFIG_NOT_ERROR if json
 * file parsed succesfuly.
 */
int get_selected_callbacks(char *buffer, config_selection *config) {
  struct json_object *parsed_json, *callbacks, *callback, *graph_reduction,
      *graph_time, *max_tasks, *critical_path, *runtime_integration;
  struct json_tokener *tok;
  size_t n_callbacks;

  // Initialize selection.
  config->task = CONFIG_NOT_SELECTED;
  config->task_create = CONFIG_NOT_SELECTED;
  config->thread = CONFIG_NOT_SELECTED;
  config->parallel = CONFIG_NOT_SELECTED;
  config->work = CONFIG_NOT_SELECTED;
  config->implicit_task = CONFIG_NOT_SELECTED;
  config->master = CONFIG_NOT_SELECTED;
  config->sync_region = CONFIG_NOT_SELECTED;
  config->target = CONFIG_NOT_SELECTED;
  config->target_submit = CONFIG_NOT_SELECTED;
  config->target_data_op = CONFIG_NOT_SELECTED;
  config->taskwait = CONFIG_NOT_SELECTED;

  // Parse buffer to json variable.
  tok = json_tokener_new();
  parsed_json = json_tokener_parse_ex(tok, buffer, -1);

  // Verify error in parse.
  if (tok->err != json_tokener_success)
    return CONFIG_ERROR;

  // Get json array callbacks.
  if (json_object_object_get_ex(parsed_json, "monitored events", &callbacks)) {
    n_callbacks = json_object_array_length(callbacks);

    // Get json array callbacks selection.
    for (int i = 0; i < n_callbacks; i++) {
      callback = json_object_array_get_idx(callbacks, i);
      if (strcmp(json_object_get_string(callback), "task") == 0)
        config->task = CONFIG_SELECTED;
      else if (strcmp(json_object_get_string(callback), "task create") == 0)
        config->task_create = CONFIG_SELECTED;
      else if (strcmp(json_object_get_string(callback), "thread") == 0)
        config->thread = CONFIG_SELECTED;
      else if (strcmp(json_object_get_string(callback), "parallel") == 0)
        config->parallel = CONFIG_SELECTED;
      else if (strcmp(json_object_get_string(callback), "work") == 0)
        config->work = CONFIG_SELECTED;
      else if (strcmp(json_object_get_string(callback), "implicit task") == 0)
        config->implicit_task = CONFIG_SELECTED;
      else if (strcmp(json_object_get_string(callback), "master") == 0)
        config->master = CONFIG_SELECTED;
      else if (strcmp(json_object_get_string(callback), "sync region") == 0)
        config->sync_region = CONFIG_SELECTED;
      else if (strcmp(json_object_get_string(callback), "target") == 0)
        config->target = CONFIG_SELECTED;
      else if (strcmp(json_object_get_string(callback), "target submit") == 0)
        config->target_submit = CONFIG_SELECTED;
      else if (strcmp(json_object_get_string(callback), "target data op") == 0)
        config->target_data_op = CONFIG_SELECTED;
      else if (strcmp(json_object_get_string(callback), "taskwait") == 0)
        config->taskwait = CONFIG_SELECTED;
    }
  } else {
    set_default_callbacks(config);
  }

  // Get the graph-time selection
  if (json_object_object_get_ex(parsed_json, "graph time", &graph_time)) {
    if (strcmp(json_object_get_string(graph_time), "yes") == 0)
      config->graph_time = CONFIG_SELECTED;
    else
      config->graph_time = CONFIG_NOT_SELECTED;
  } else {
    config->graph_time = CONFIG_SELECTED;
  }

  // Get the max_tasks selection
  if (json_object_object_get_ex(parsed_json, "max tasks", &max_tasks)) {
    config->max_tasks = json_object_get_int(max_tasks);
    if (config->max_tasks <= 0)
      config->max_tasks = MAX_TASKS_DEFAULT;
  } else {
    config->max_tasks = MAX_TASKS_DEFAULT;
  }

  // Get the critical-path selection
  if (json_object_object_get_ex(parsed_json, "critical path", &critical_path)) {
    if (strcmp(json_object_get_string(critical_path), "yes") == 0)
      config->critical_path = CONFIG_SELECTED;
    else
      config->critical_path = CONFIG_NOT_SELECTED;
  } else {
    config->critical_path = CONFIG_NOT_SELECTED;
  }

  // Get the runtime-integration selection
  if (getenv("LIBOMPTARGET_PROFILE") != NULL)
    config->runtime_integration = CONFIG_SELECTED;
  else
    config->runtime_integration = CONFIG_NOT_SELECTED;

  return CONFIG_NOT_ERROR;
}
