#include "graph.h"
#include <string.h>
#include <unistd.h>

/*!
  \var fp_dp
  Pointer to graph dependence file.
*/
FILE *fp_dp;

/*!
  \var finish_graph
  Indicates when writing of the graph file is finished (FINISH_WRITE) or didn't
  finish (NOT_FINISH_WRITE).
*/
static int finish_graph;

/*!
  \var dependences_array
  Keeps task dependences when graph reduction is selected.
*/
static char **dependences_array;

/*!
  \var dependences_array
  Keeps task dependences when graph reduction is selected.
*/
static task_array_data *task_array;

/*!
 * Add dependences to dependence list.
 * @param id_sink is the id of the sink task.
 * @param id_source is the id of the source task.
 * @param dp_list is the pointer to dependence list.
 */
void add_dependence(unsigned int id_sink, unsigned int id_source,
                    dependence_list *dp_list) {
  pthread_mutex_lock(&(dp_list->mutex));

  if (dp_list->graph_reduction == GRAPH_REDUCTION_NOT_SELECTED &&
      dp_list->critical_path == CRITICAL_PATH_NOT_SELECTED) {
    dependence *dp;
    if (dp_list->next == NULL) {
      // First dependence.
      dp_list->next = (dependence *)malloc(sizeof(dependence));
      dp = dp_list->next;
      dp_list->head = dp_list->next;
    } else {
      // Atualize pointers.
      dp = dp_list->next;
      dp->next = (dependence *)malloc(sizeof(dependence));
      dp_list->next = dp->next;
      dp = dp->next;
    }

    // Add dependence.
    dp->next = NULL;
    dp->id_sink = id_sink;
    dp->id_source = id_source;
  } else {
    if (id_source >= dp_list->dependence_size ||
        id_sink >= dp_list->dependence_size) {
      printf("The number of tasks is greater than the selected max_tasks. "
             "Some tasks will not be recorded in the graph.\n");
    } else {
      dependences_array[id_source][id_sink] = 1;

      if (dp_list->critical_path == CRITICAL_PATH_SELECTED)
        task_array[id_sink].root = NOT_ROOT_TASK;
    }
  }

  pthread_mutex_unlock(&(dp_list->mutex));
}

/*!
 * The thread function to write dependences.
 * @param dependences is the pointer to dependence list.
 */
void *write_dependences(void *dependences) {
  dependence_list *dp_list = (dependence_list *)dependences;
  dp_list->writer = pthread_self();
  int error_write = ERROR_NOT_OCCUR;
  finish_graph = NOT_FINISH_WRITE;

  while (finish_graph == NOT_FINISH_WRITE && error_write == ERROR_NOT_OCCUR) {
    dependence *dp = dp_list->head;

    pthread_mutex_lock(&(dp_list->mutex));
    while (dp != NULL) {

      // Print dependence.
      if (fprintf(fp_dp, "\t%d -> %d;\n", dp->id_source, dp->id_sink) < 0) {
        error_write = ERROR_OCCURRED;
        break;
      }

      // Get next dependence write.
      dp = dp->next;
      dp_list->head = dp;
    }

    dp_list->next = dp_list->head;

    pthread_mutex_unlock(&(dp_list->mutex));
  }

  // Verify error in graph file write.
  if (error_write == ERROR_OCCURRED)
    printf("Can't write graph file.\n");

  return NULL;
}

/*!
 * The dfs function to reduce the graph.
 * @param node_cmp is the root node of the dfs.
 * @param node is the current node of the dfs.
 * @param dp_list is the dependence list.
 * @param nodes_visited keep the nodes already visited.
 */
void dfs_graph_reduction(int node_cmp, int node, dependence_list *dp_list,
                         char *nodes_visited) {
  // Check all edges of the current node

  // Begin visit
  nodes_visited[node] = 1;
  for (int i = 0; i <= dp_list->max_task_id; i++) {
    // Graph reduction
    if (dependences_array[node][i] == 1 &&
        dependences_array[node_cmp][i] == 1 && node_cmp != node) {
      dependences_array[node_cmp][i] = 0;
    }

    // Continue the next level of the dfs
    if (dependences_array[node][i] != 0 && nodes_visited[i] == 0)
      dfs_graph_reduction(node_cmp, i, dp_list, nodes_visited);

    // Write the task graph
    if (dependences_array[node_cmp][i] == 1 && node_cmp == node &&
        dp_list->critical_path == CRITICAL_PATH_NOT_SELECTED) {
      fprintf(fp_dp, "\t%d -> %d;\n", node_cmp, i);
    }
  }
  // Finish visit
  nodes_visited[node] = 2;
}

/*!
 * Calculate the longest path of each task of graph.
 * @param root is the id of root task.
 * @param dp_list is the pointer to dependence list.
 */
void calculate_longest_path(unsigned int root, dependence_list *dp_list) {
  if (task_array[root].path_time != -1)
    return;

  task_array_data task_longest_path;
  task_longest_path.path_time = 0;
  for (unsigned int i = 0; i <= dp_list->max_task_id; i++) {
    if (dependences_array[root][i] == 1) {
      calculate_longest_path(i, dp_list);
      if (task_array[i].path_time >= task_longest_path.path_time) {
        task_longest_path = task_array[i];
        task_array[root].next_child_longest_path = i;
      }
    }
  }

  task_array[root].path_time =
      task_array[root].elapsed_time + task_longest_path.path_time;
}

/*!
 * Set the critical path.
 * @param root is the id of root task.
 */
void set_critical_path(unsigned int root) {
  task_array[root].critical_path = CRITICAL_PATH;
  if (task_array[root].next_child_longest_path != -1)
    set_critical_path(task_array[root].next_child_longest_path);
}

/*!
 * The function to write the dependences with critical path.
 * @param root is the current node.
 * @param dp_list is the dependence list.
 */
void write_dependences_task_data(unsigned int root, dependence_list *dp_list) {
  if (task_array[root].visited == VISITED)
    return;

  task_array[root].visited = VISITED;
  for (int i = 0; i <= dp_list->max_task_id; i++) {
    if (dependences_array[root][i] == 1) {
      if (task_array[root].critical_path == CRITICAL_PATH &&
          task_array[i].critical_path == CRITICAL_PATH) {
        fprintf(fp_dp, "\t%d -> %d [color=blue];\n", root, i);
        fprintf(fp_dp, "\t%d [color=blue, fontcolor=\"blue\"];\n", root);
        fprintf(fp_dp, "\t%d [color=blue, fontcolor=\"blue\"];\n", i);

      } else {
        fprintf(fp_dp, "\t%d -> %d;\n", root, i);
      }
      write_dependences_task_data(i, dp_list);
    }
  }
}

/*!
 * Finalize the dependence list.
 * @param dp_list is the pointer to dependence list.
 */
void finalize_dependence(dependence_list *dp_list) {
  // Finalize list.
  pthread_mutex_lock(&(dp_list->mutex));

  // Graph reduction
  if (dp_list->graph_reduction == GRAPH_REDUCTION_SELECTED) {
    char *nodes_visited =
        (char *)malloc((dp_list->max_task_id + 1) * sizeof(char));
    for (int i = 0; i <= dp_list->max_task_id; i++) {
      for (int j = 0; j <= dp_list->max_task_id; j++)
        nodes_visited[j] = 0;
      dfs_graph_reduction(i, i, dp_list, nodes_visited);
    }
  }

  // Write graph
  if (dp_list->critical_path == CRITICAL_PATH_SELECTED) {
    unsigned int critical_path_root = -1;
    // Calculate longest path
    for (int i = 0; i <= dp_list->max_task_id; i++) {
      if (task_array[i].root == ROOT_TASK) {
        calculate_longest_path(i, dp_list);
        if (critical_path_root == -1)
          critical_path_root = i;
        else if (task_array[i].path_time >
                 task_array[critical_path_root].path_time)
          critical_path_root = i;
      }
    }
    if (critical_path_root != -1)
      set_critical_path(critical_path_root);

    // Write dependences
    for (int i = 0; i <= dp_list->max_task_id; i++) {
      if (task_array[i].root == ROOT_TASK) {
        write_dependences_task_data(i, dp_list);
      }
    }
  }

  // Verify error in graph file write.
  if (fprintf(fp_dp, "}") < 0)
    printf("Can't write graph file.\n");

  fclose(fp_dp);

  pthread_mutex_unlock(&(dp_list->mutex));
  finish_graph = FINISH_WRITE;
}

/*!
 * Initialize dependence list.
 * @param dp_list is the pointer to dependence list.
 * @return ERROR_OCCURRED if a error occurred in initialize the dependence list
 * or ERROR_NOT_OCCURED if the graph was succesfully initialized.
 */
int initialize_dependence(dependence_list *dp_list) {
  if (dp_list->graph_reduction == GRAPH_REDUCTION_NOT_SELECTED &&
      dp_list->critical_path == CRITICAL_PATH_NOT_SELECTED) {
    dp_list->next = NULL;
    dp_list->head = NULL;
  }
  pthread_mutex_init(&(dp_list->mutex), NULL);

  int error = ERROR_NOT_OCCUR;

  // Create file.
  char *str = (char *)malloc(20 * sizeof(char));
  sprintf(str, "graph_%ld.gv", (long int)getpid());

  fp_dp = fopen(str, "w");
  if (fp_dp == NULL)
    error = ERROR_OCCURRED;

  // Initialize graph file.
  if (fprintf(fp_dp, "digraph task_dependence {\n") < 0)
    error = ERROR_OCCURRED;

  if (dp_list->graph_reduction == GRAPH_REDUCTION_SELECTED ||
      dp_list->critical_path == CRITICAL_PATH_SELECTED) {

    dependences_array =
        (char **)malloc(dp_list->dependence_size * sizeof(char *));

    for (int i = 0; i < dp_list->dependence_size; i++) {
      dependences_array[i] =
          (char *)malloc(dp_list->dependence_size * sizeof(char));
      memset(dependences_array[i], 0, sizeof(char) * dp_list->dependence_size);
    }
  }

  if (dp_list->critical_path == CRITICAL_PATH_SELECTED) {
    task_array = (task_array_data *)malloc(dp_list->dependence_size *
                                           sizeof(task_array_data));

    for (int i = 0; i < dp_list->dependence_size; i++) {
      task_array[i].root = ROOT_TASK;
      task_array[i].visited = NOT_VISITED;
      task_array[i].next_child_longest_path = -1;
      task_array[i].path_time = -1;
      task_array[i].critical_path = NOT_CRITICAL_PATH;
    }
  }

  return error;
}

/*!
 * Add time_task to task time list.
 * @param id is the id of the time task.
 * @param begin is the begin time of the task.
 * @param end is the end time of the task.
 * @param tk_time_list is the pointer to time task list.
 * @param taskwait indicates if is a TASKWAIT_TYPE or a NOT_TASKWAIT_TYPE.
 */
void add_time_task(unsigned int id, double begin, double end,
                   time_task_list *tk_time_list, int taskwait) {
  pthread_mutex_lock(&(tk_time_list->mutex));

  time_task *tk;
  if (tk_time_list->next == NULL) {
    // First time task.
    tk_time_list->next = (time_task *)malloc(sizeof(time_task));
    tk = tk_time_list->next;
    tk_time_list->head = tk_time_list->next;
  } else {
    // Search last time task.
    tk = tk_time_list->next;
    while (tk->next != NULL) {
      tk = tk->next;
    }
    // Atualize pointers.
    tk->next = (time_task *)malloc(sizeof(time_task));
    tk = tk->next;
  }

  // Add dependence.
  tk->next = NULL;
  tk->id = id;
  tk->begin = begin;
  tk->end = end;
  tk->taskwait = taskwait;

  pthread_mutex_unlock(&(tk_time_list->mutex));
}

/*!
 * Add elapsed time to task_array.
 * @param id is the id of the task.
 * @param elapsed_time is the elapsed time of the task.
 * @param dp_list is the pointer to dependence list.
 */
void add_elapsed_time(unsigned int id, double elapsed_time,
                      dependence_list *dp_list) {
  if (id >= dp_list->dependence_size) {
    printf("The number of tasks is greater than the selected max_tasks. "
           "Some tasks will not be recorded in the graph.\n");
  } else {
    if (elapsed_time == 0)
      elapsed_time = 0.0000001;
    task_array[id].elapsed_time = elapsed_time;
  }
}

/*!
 * The thread function to write time list.
 * @param tasks is the pointer to time task list.
 */
void *write_time_list(void *tasks) {
  time_task_list *tk_list = (time_task_list *)tasks;
  tk_list->writer = pthread_self();
  int error_write = ERROR_NOT_OCCUR;
  tk_list->finish_time_list = NOT_FINISH_WRITE;

  while (tk_list->finish_time_list == NOT_FINISH_WRITE &&
         error_write == ERROR_NOT_OCCUR) {

    if (pthread_mutex_trylock(&(tk_list->mutex)) != EBUSY) {
      time_task *tk = tk_list->head;
      while (tk != NULL) {

        // Print task time.
        if (tk->taskwait == NOT_TASKWAIT_TYPE) {
          if (fprintf(fp_dp,
                      "\t%d [label= <%d <BR /><FONT POINT-SIZE=\"10\">Begin: "
                      "%.2lf</FONT> <BR /><FONT POINT-SIZE=\"10\">End: "
                      "%.2lf</FONT> "
                      "<BR /><FONT POINT-SIZE=\"10\">Elapsed Time: "
                      "%.2lf</FONT>>];\n",
                      tk->id, tk->id, tk->begin, tk->end,
                      tk->end - tk->begin) < 0) {
            error_write = ERROR_OCCURRED;
            pthread_mutex_unlock(&(tk_list->mutex));
            break;
          }
        } else {
          if (fprintf(fp_dp,
                      "\t%d [label= <%d <BR /><FONT POINT-SIZE=\"10\">Begin: "
                      "%.2lf</FONT> <BR /><FONT POINT-SIZE=\"10\">End: "
                      "%.2lf</FONT> "
                      "<BR /><FONT POINT-SIZE=\"10\">Elapsed Time: %.2lf</FONT>"
                      "<BR /><FONT POINT-SIZE=\"10\" "
                      "color=\"red\">Taskwait</FONT>>];\n",
                      tk->id, tk->id, tk->begin, tk->end,
                      tk->end - tk->begin) < 0) {
            error_write = ERROR_OCCURRED;
            pthread_mutex_unlock(&(tk_list->mutex));

            break;
          }
        }

        // Get next task write.
        tk = tk->next;
        tk_list->head = tk;
      }

      tk_list->next = tk_list->head;

      pthread_mutex_unlock(&(tk_list->mutex));
    }
  }

  tk_list->finish_thread_time = 1;

  // Verify error in graph file write.
  if (error_write == ERROR_OCCURRED)
    printf("Can't write graph file.\n");

  pthread_exit((void *)0);
}

/*!
 * Finalize the time task list.
 * @param tk_time_list is the pointer to time task list.
 */
void finalize_time_list(time_task_list *tk_time_list) {
  // Finalize list.
  tk_time_list->finish_time_list = FINISH_WRITE;
}

/*!
 * Initialize time task list.
 * @param tk_time_list is the pointer to time task list.
 * @return ERROR_OCCURRED if a error occurred in initialize the time task list
 * or ERROR_NOT_OCCURED if the time task list was succesfully initialized.
 */
int initialize_time_list(time_task_list *tk_time_list) {
  tk_time_list->next = NULL;
  tk_time_list->head = NULL;

  // Verify error
  int error = ERROR_NOT_OCCUR;
  if (fp_dp == NULL)
    error = ERROR_OCCURRED;

  return error;
}

/*!
 * Add tasks to task data list.
 * @param task is the ompt data pointer.
 * @param id is the task omptracing id.
 * @param value is the ompt id.
 * @param tk_list is the pointer to ompt task list.
 */
void add_task_data(ompt_data_t *task, uint64_t id, uint64_t value,
                   task_data_list *tk_list) {
  pthread_mutex_lock(&(tk_list->mutex));

  ompt_task_data *tk;

  if (tk_list->next == NULL) {
    // First task.
    tk_list->next = (ompt_task_data *)malloc(sizeof(ompt_task_data));
    tk = tk_list->next;
    tk->next = NULL;
    tk->value = value;
    tk->id = id;
    tk->tk_data = ((void *)task->ptr);
    tk_list->head = tk_list->next;
  } else {
    tk = tk_list->next;
    while (tk->next != NULL) {
      tk = tk->next;
    }
    // Atualize pointers.
    tk->next = (ompt_task_data *)malloc(sizeof(ompt_task_data));
    (tk->next)->tk_data = ((void *)task->ptr);
    (tk->next)->value = value;
    (tk->next)->id = id;
    (tk->next)->next = NULL;
  }

  tk_list->last_id = id;

  pthread_mutex_unlock(&(tk_list->mutex));
}

/*!
 * Check if has a path between two tasks.
 * @param sink is the id of sink task.
 * @param src is the id of source task.
 * @param dp_list is the pointer to dependence list.
 * @return HAS_PATH if has a path between sink task and source task or
 * HAS_NOT_PATH otherwise.
 */

int check_path(uint64_t sink, uint64_t src, dependence_list *dp_list) {
  if (sink >= dp_list->dependence_size || src >= dp_list->dependence_size) {
    printf("The number of tasks is greater than the selected max_tasks. "
           "Some tasks will not be recorded in the graph.\n");
    return -1;
  }

  if (dependences_array[sink][src] == 1)
    return HAS_PATH;

  for (int i = 0; i <= dp_list->max_task_id; i++) {
    // Graph reduction
    if (dependences_array[sink][i] == 1) {
      return check_path(i, src, dp_list);
    }
  }

  return HAS_NOT_PATH;
}

/*!
 * Search out tasks to add dependence.
 * @param task is the ompt data pointer.
 * @param id is the task omptracing id.
 * @param value is the ompt id.
 * @param tk_list is the pointer to ompt task list.
 * @param dp_list is the pointer to dependence list.
 * @return the number of dependencies added.
 */
int search_out_dep(ompt_data_t *task, uint64_t id, uint64_t value,
                   task_data_list *tk_list, dependence_list *dp_list) {
  int dep_count = 0;
  pthread_mutex_lock(&(tk_list->mutex));

  ompt_task_data *tk = tk_list->next;
  ompt_task_data *tk_previous = NULL;
  ompt_task_data *last_out = NULL;
  ompt_task_data *last_out_prev = NULL;

  // Search for the respective dependence
  while (tk != NULL) {
    if (tk->value == value && tk->tk_data != task->ptr) {
      if (last_out != NULL) {
        // Remove
        if (last_out_prev == NULL) {
          tk_list->next = last_out->next;
        } else {
          last_out_prev->next = last_out->next;
        }
      }

      last_out = tk;
      last_out_prev = tk_previous;
    }
    // Atualize pointers
    tk_previous = tk;
    tk = tk->next;
  }

  if (last_out != NULL) {
    dep_count++;
    add_dependence(id, last_out->id, dp_list);
  }

  pthread_mutex_unlock(&(tk_list->mutex));

  return dep_count;
}

/*!
 * Search in tasks to add dependence.
 * @param task is the ompt data pointer.
 * @param id is the task omptracing id.
 * @param value is the ompt id.
 * @param tk_list is the pointer to ompt task list.
 * @param dp_list is the pointer to dependence list.
 * @return the number of dependencies added.
 */
int search_in_dep(ompt_data_t *task, uint64_t id, uint64_t value,
                  task_data_list *tk_list, dependence_list *dp_list) {
  int dep_count = 0, task_removed = 0;
  pthread_mutex_lock(&(tk_list->mutex));

  ompt_task_data *tk = tk_list->next;
  ompt_task_data *tk_previous = NULL;

  // Search for the respective dependence
  while (tk != NULL) {
    if (tk->value == value && tk->tk_data != task->ptr) {
      add_dependence(tk->id, id, dp_list);
      dep_count++;
      // Remove task
      task_removed = 1;
      if (tk_previous == NULL) {
        // First task
        tk_list->next = tk->next;
      } else {
        tk_previous->next = tk->next;
      }
    }
    // Atualize pointers
    if (task_removed == 1) {
      task_removed = 0;
    } else
      tk_previous = tk;
    tk = tk->next;
  }

  pthread_mutex_unlock(&(tk_list->mutex));

  return dep_count;
}

/*!
 * Initialize task data list.
 * @param tk_list is the pointer to task data list.
 */
void initialize_task_data_list(task_data_list *tk_list) {
  tk_list->next = NULL;
  tk_list->head = NULL;
  pthread_mutex_init(&(tk_list->mutex), NULL);
}

/*!
 * Add taskwait dependences out.
 * @param tk_list is the pointer to ompt task list.
 * @param dp_list is the pointer to dependence list.
 * @param begin_time is the array of begin time task.
 */
void add_taskwait_dependences(task_data_list *tk_list, dependence_list *dp_list,
                              double *begin_time) {
  pthread_mutex_lock(&(tk_list->mutex));

  ompt_task_data *tk = tk_list->next;

  while (tk != NULL) {
    for (int i = 0; i < dp_list->max_task_id; i++) {
      if (begin_time[i] == -1)
        break;
      if (begin_time[i] > begin_time[tk->id])
        add_dependence(i, tk->id, dp_list);
      if (begin_time[i] < begin_time[tk->id])
        add_dependence(tk->id, i, dp_list);
    }
    tk = tk->next;
  }

  pthread_mutex_unlock(&(tk_list->mutex));
}

/*!
 * Verify if taskwait already exists in task data list.
 * @param id is the task omptracing id.
 * @param tk_list is the pointer to ompt task list.
 */
int taskwait_exists(uint64_t id, task_data_list *tk_list) {
  int exists = 0;
  pthread_mutex_lock(&(tk_list->mutex));

  if (id <= tk_list->last_id)
    exists = 1;

  pthread_mutex_unlock(&(tk_list->mutex));
  return exists;
}
