#include "tracing.h"

#include <unistd.h>

/*!
  \var fp
  Pointer to JSON file.
*/
FILE *fp;

/*!
  \var first
  Indicates that the next task to be written is the first (FIRST_TASK) or is not
  the first (NOT_FIRST_TASK). \var finish Indicates when writing of the trace
  file is finished (FINISH_WRITE) or didn't finish (NOT_FINISH_WRITE).
*/
static int first, finish;

/*!
 * Add args in arg list.
 * @param args is the pointer to arg list.
 * @param label is the arg name.
 * @param value is the arg value.
 * @return The atualized arg list.
 */
arg *add_args(arg *args, char *label, void *value, task_list *tk_list) {
  pthread_mutex_lock(&(tk_list->mutex));

  arg *ar = args;

  if (args == NULL) {
    // Initialize arg list.
    args = (arg *)malloc(sizeof(arg));
    ar = args;
  } else {
    // Create next arg.
    while (ar->next != NULL) {
      ar = ar->next;
    }
    ar->next = (arg *)malloc(sizeof(arg));
    ar = ar->next;
  }

  // Add arg.
  ar->label = label;
  ar->value = value;
  ar->next = NULL;

  pthread_mutex_unlock(&(tk_list->mutex));

  return args;
}

/*!
 * Add tasks to task list.
 * @param cat is the event category. In this application your value is always
 * OMPT.
 * @param pid is the event processor ID. In this application your value is the
 * thread number.
 * @param tid is the event thread ID. In this application your value is the
 * region type.
 * @param name is the event name. In this application your value is the region
 * type concatenated with region id.
 * @param ph is the event phase or event type. In this application your value is
 * 'B' (Begin) or 'E' (End).
 * @param ts is the time that event was created in ms.
 * @param args is any information about event.
 * @param tk_list is the pointer to task list.
 */
void add_task(char *cat, int pid, char *tid, char *name, char ph, double ts,
              arg *args, task_list *tk_list) {
  pthread_mutex_lock(&(tk_list->mutex));

  task *tk;
  if (tk_list->next == NULL) {
    // First task.
    tk_list->next = (task *)malloc(sizeof(task));
    tk = tk_list->next;
    tk_list->head = tk_list->next;
  } else {
    // Atualize pointers.
    tk = tk_list->next;
    tk->next = (task *)malloc(sizeof(task));
    tk_list->next = tk->next;
    tk = tk->next;
  }

  // Add task.
  tk->next = NULL;
  tk->cat = cat;
  tk->pid = pid;
  tk->tid = tid;
  tk->name = name;
  tk->ph = ph;
  tk->ts = ts;
  tk->args = args;

  pthread_mutex_unlock(&(tk_list->mutex));
}

/*!
 * The thread function to write tasks.
 * @param tasks is the pointer to task list.
 */
void *write_tasks(void *tasks) {
  task_list *tk_list = (task_list *)tasks;
  tk_list->writer = pthread_self();
  int error_write = ERROR_NOT_OCCUR;
  finish = NOT_FINISH_WRITE;
  long int pid;

  if (tk_list->runtime_integration == RUNTIME_INTEGRATION_SELECTED)
    pid = (long int)getpid();

  while (finish == NOT_FINISH_WRITE && error_write == ERROR_NOT_OCCUR) {
    pthread_mutex_lock(&(tk_list->mutex));
    task *tk = tk_list->head;
    while (tk != NULL) {

      // Verify first task.
      if (first == FIRST_TASK) {
        first = NOT_FIRST_TASK;
      } else {
        if (fprintf(fp, ",\n") < 0) {
          error_write = ERROR_OCCURRED;
          break;
        }
      }

      // Print task information.
      if (fprintf(fp, "{\n") < 0) {
        error_write = ERROR_OCCURRED;
        break;
      }
      if (fprintf(fp, "\t\"cat\": \"%s\",\n", tk->cat) < 0) {
        error_write = ERROR_OCCURRED;
        break;
      }
      if (tk_list->runtime_integration == RUNTIME_INTEGRATION_NOT_SELECTED) {
        if (tk->pid < 0) {
          if (fprintf(fp, "\t\"pid\": \"All\",\n") < 0) {
            error_write = ERROR_OCCURRED;
            break;
          }
        } else {
          if (fprintf(fp, "\t\"pid\": \"Thread %d\",\n", tk->pid) < 0) {
            error_write = ERROR_OCCURRED;
            break;
          }
        }
      } else {
        if (fprintf(fp, "\t\"pid\": \"%ld\",\n", pid) < 0) {
          error_write = ERROR_OCCURRED;
          break;
        }
      }
      if (fprintf(fp, "\t\"tid\": \"%s\",\n", tk->tid) < 0) {
        error_write = ERROR_OCCURRED;
        break;
      }
      if (fprintf(fp, "\t\"ts\": %lf,\n", tk->ts) < 0) {
        error_write = ERROR_OCCURRED;
        break;
      }
      if (fprintf(fp, "\t\"ph\": \"%c\",\n", tk->ph) < 0) {
        error_write = ERROR_OCCURRED;
        break;
      }
      if (fprintf(fp, "\t\"name\": \"%s\",\n", tk->name) < 0) {
        error_write = ERROR_OCCURRED;
        break;
      }
      if (fprintf(fp, "\t\"args\": {\n") < 0) {
        error_write = ERROR_OCCURRED;
        break;
      }

      // Print args list.
      arg *args = tk->args;
      while (args != NULL) {
        if (args->next != NULL) {
          if (fprintf(fp, "\t\t\"%s\": \"%s\",\n", args->label,
                      (char *)args->value) < 0) {
            error_write = ERROR_OCCURRED;
            break;
          }
        } else {
          if (fprintf(fp, "\t\t\"%s\": \"%s\"\n", args->label,
                      (char *)args->value) < 0) {
            error_write = ERROR_OCCURRED;
            break;
          }
        }
        args = args->next;
      }
      if (fprintf(fp, "\t}\n") < 0) {
        error_write = ERROR_OCCURRED;
        break;
      }

      // Finalize this task write.
      if (fprintf(fp, "}") < 0) {
        error_write = ERROR_OCCURRED;
        break;
      }

      // Get next task write.
      tk = tk->next;
      tk_list->head = tk;
    }

    tk_list->next = tk_list->head;

    pthread_mutex_unlock(&(tk_list->mutex));
  }

  // Verify error in json file write.
  if (error_write == ERROR_OCCURRED)
    printf("Can't write omptracing file.\n");

  return NULL;
}

/*!
 * Finalize the task list.
 * @param tk_list is the pointer to task list.
 */
void finalize_list(task_list *tk_list) {
  // Finalize list.
  pthread_mutex_lock(&(tk_list->mutex));

  // Verify error in json file write.
  if (fprintf(fp, "\n]") < 0)
    printf("Can't write omptracing file.\n");

  fclose(fp);
  pthread_mutex_unlock(&(tk_list->mutex));
  finish = FINISH_WRITE;
}

/*!
 * Initialize task list.
 * @param tk_list is the pointer to task list.
 * @param error is the error pointer.
 */
void initialize_list(task_list *tk_list, int *error) {
  tk_list->next = NULL;
  tk_list->head = NULL;
  tk_list->runtime_integration = RUNTIME_INTEGRATION_NOT_SELECTED;
  first = FIRST_TASK;
  pthread_mutex_init(&(tk_list->mutex), NULL);

  *error = ERROR_NOT_OCCUR;

  // Create file.
  char *str = (char *)malloc(20 * sizeof(char));
  sprintf(str, "omptracing_%ld.json", (long int)getpid());

  fp = fopen(str, "w");
  if (fp == NULL)
    *error = ERROR_OCCURRED;

  // Initialize json file.
  if (fprintf(fp, "[\n") < 0)
    *error = ERROR_OCCURRED;
}
