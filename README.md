# OmpTracing

Tracing tool for OpenMP programs.

## Requirements

An OpenMP runtime with a compatible OMPT need to be available.
For now, OmpTracing only support Clang v11.x.
OmpTracing is tested against the current release of our [LLVM fork](https://gitlab.com/ompcluster/llvm-project).

The json-c library need to be installed on your system as well.

```bash
sudo apt-get install libjson-c-dev
```

## Build

This software uses CMake 3.10+ as its build system generator. On your terminal,
run the following commands to compile the source code:

```bash
mkdir build
cd build
cmake -DCMAKE_BUILD_TYPE=Release ..
make
```

This will produce the `libomptracing.so` library file.

If you want to generate the Doxygen documentation then set `-DBUILD_DOCUMENTATION` to 1.
If you want to use OpenMP target then set `-DOPENMP_TARGETS` to the triples of the target devices (be aware this feature is still experimental).

## Usage

Start by setting the environment variable `OMP_TOOL_LIBRARIES` to libomptracing.so path.
Then, compile and run your application and OmpTracing will generate both JSON and DOT files representing the execution trace and the task graph respectively.

To test the tool, you can build and run the application in the example folder.  

More instructions on how to use OmpTracing are available [here](https://ompcluster.readthedocs.io/en/latest/omptracing.html).
